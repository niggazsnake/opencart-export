# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import shutil
from zipfile import ZipFile, ZIP_DEFLATED
from config import ROOT_TEMP_FOLDER, ARCHIVES_TEMP_FOLDER


class Archive(object):
    def __init__(self, file_path):
        self.path = file_path
        self.path_saved = None

    def clean_export_folder(self):
        path = os.path.dirname(self.path)
        if os.path.exists(path):
            for the_file in os.listdir(path):
                file_path = os.path.join(path, the_file)
                try:
                    if os.path.isfile(file_path):
                        os.unlink(file_path)
                    elif os.path.isdir(file_path):
                        shutil.rmtree(file_path)
                except Exception as e:
                    print(e)

    def create_archive(self):
        if self.path and os.path.exists(self.path):
            arch_name = os.path.basename(self.path)
            name, ext = os.path.splitext(arch_name)
            path_to_arch = os.path.dirname(self.path)
            path_save_arch = os.path.join(ROOT_TEMP_FOLDER, ARCHIVES_TEMP_FOLDER, name)
            self.path_saved = "%s.zip" % path_save_arch
            shutil.make_archive(path_save_arch, 'zip', path_to_arch)

            self.clean_export_folder()

            print 'archive create'
        else:
            print 'error on creating archive'
