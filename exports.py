# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import os
from shutil import copyfile
from config import FILENAME_CATALOG_TEMPLATE_XML, EXPORT_TEMP_FOLDER, ROOT_TEMP_FOLDER, FILENAME_OFFERS_TEMPLATE_XML
from helpers import XMLHelper


class CommerceExport(object):
    SHOP_NAME = 'Магазин'
    SHOP_ID = 'shop-id-1'
    SHOP_OKPO = ''
    CLASSIFIER_ID = 'classifier-id-1'
    CLASSIFIER_NAME = 'Классификатор'
    CATALOG_ID = 'catalog-id-1'
    TEMPLATE_NAME = FILENAME_CATALOG_TEMPLATE_XML

    def __init__(self, index_file, products=None, category_last_pk=1, product_last_pk=1):
        namespaces = {
            'xs': 'http://www.w3.org/2001/XMLSchema',
            'xsi': 'http://www.w3.org/2001/XMLSchema-instance'

        }
        # self._filename = filename
        self.index_file = index_file
        self.export_folder = self.get_export_path()
        self.xml_export_path = self.move_xml_template()
        self.category_last_pk = category_last_pk
        self.product_last_pk = product_last_pk
        self.categories = {}
        self.groups = None
        self.products = products if products is not None else []
        self.product_groups = {}
        root_attr = {'xmlns': 'urn:1C.ru:commerceml_2', 'ВерсияСхемы': '2.07', 'ДатаФормирования': self.today}

        self.helper = XMLHelper('КоммерческаяИнформация', namespaces, root_attr)

        self.header = self.add_header()
        self.add_common()

    def add_common(self):
        self.groups = self.add_content(self.header)
        self.products = self.add_products()

    def save_xml(self):
        self.helper.save_xml(self.xml_export_path)

    def add_category(self, category_parent, category_child):
        parent = self.categories.get(category_parent, None)
        if parent is None:
            self.categories[category_parent] = {}

        child = parent.get(category_child, None) if parent else None
        if child is None:
            self.categories[category_parent][category_child] = self.category_last_pk
            self.category_last_pk += 1
        return self.category_name_new("child", self.category_last_pk-1)

    def get_category_id(self, parent, child):
        if parent and child:
            parent_el = self.categories.get(parent, None)
            if parent_el:
                return parent_el.get(child, -1)
        return -1

    def get_child_name_id(self, d):
        if d:
            for name, id_child in d.items():
                return [name, id_child]
        return [0, 0]

    def category_name_new(self, type_category, id_category):
        return "group-%s-%s" % (id_category, type_category)

    def add_categories_to_xml(self):
        if self.categories:
            for key, children in self.categories.items():
                name_child, id_child = self.get_child_name_id(children) if children else [None, 0]
                el = self.add_group(self.groups, key, self.category_name_new("parent", id_child))

                if children:
                    groups = self.helper.add_el(el, 'Группы', 'notext')
                    for name, id_child in children.items():
                        self.add_group(groups, name, self.category_name_new("child", id_child))

    def add_item(self, category_parent, category_child, bar_code, pk_product, name_product):
        pass

    def get_export_path(self):
        end_folder_export = "%s%d" % (EXPORT_TEMP_FOLDER, self.index_file)
        folder_path = os.path.join(ROOT_TEMP_FOLDER, EXPORT_TEMP_FOLDER, end_folder_export)
        if not os.path.exists(folder_path):
            os.makedirs(folder_path)
        return folder_path

    def create_new_name(self):
        name, ext = os.path.splitext(self.TEMPLATE_NAME)
        name = list(name)
        name[len(name) - 1] = str(self.index_file)
        name = ''.join(name)
        return "%s%s" % (name, ext)

    def move_xml_template(self):
        template_path = os.path.join('templates', self.TEMPLATE_NAME)
        new_filename = os.path.join(self.export_folder, self.create_new_name())

        if os.path.exists(template_path):
            copyfile(template_path, new_filename)

        return new_filename

    def get_save_path(self):
        return self.xml_export_path

    # def get_file(self):
    # return self._filename

    def add_header(self):
        cls = self.helper.add_el(self.helper.root_el, 'Классификатор', 'notext')
        self.helper.add_el(cls, 'Ид', self.CLASSIFIER_ID)
        self.helper.add_el(cls, 'Наименование', self.CLASSIFIER_NAME)
        self.add_owner(cls)
        return cls

    def add_content(self, parent=None):
        el = parent if parent is not None else self.helper.root_el
        groups = self.helper.add_el(el, 'Группы', 'notext')
        return groups

    @property
    def today(self):
        date = datetime.datetime.now()
        return str(date.isoformat(sep=b'T'))

    @staticmethod
    def category_name(pk):
        return "group-id-%d" % pk

    @staticmethod
    def product_name(pk):
        return "product-id-%d" % pk

    def add_group(self, parent, name, id):
        group = self.helper.add_el(parent, 'Группа', 'notext')
        self.helper.add_el(group, 'Ид', id)
        self.helper.add_el(group, 'Наименование', name)
        return group

    def parse_tree(self, tree=None, parent=None):
        if tree:
            for item in tree:
                data = item.get('data', None)
                if data:
                    name = data.get('name', None)
                    id = item.get('id', None)
                    group = self.add_group(parent, name, id)
                    children = item.get('children')
                    if children:
                        groups = self.helper.add_el(group, 'Группы', 'notext')
                        self.parse_tree(children, groups)

    def skip_first_el_in_catalog(self, tree):
        return tree[0].get('children', None)

    def add_requisite(self, parent, name, value):
        requisite = self.helper.add_el(parent, 'ЗначениеРеквизита', 'notext')
        self.helper.add_el(requisite, 'Наименование', name)
        self.helper.add_el(requisite, 'Значение', value)

    def add_base_one_price(self, parent):
        base_el = self.helper.add_el(parent, 'БазоваяЕдиница', 'notext')
        attr = {'Код': "796", 'НаименованиеПолное': "Штука", 'МеждународноеСокращение': "PCE"}
        self.helper.add_attr(base_el, attr)
        recount = self.helper.add_el(base_el, 'Пересчет', 'notext')
        self.helper.add_el(recount, 'Единица', 796)
        self.helper.add_el(recount, 'Коэффициент', 1)

    def add_products(self):
        catalog = self.helper.add_el(self.helper.root_el, 'Каталог', 'notext')
        self.helper.add_attr2(catalog, 'СодержитТолькоИзменения', 'false')
        self.helper.add_el(catalog, 'Ид', self.CLASSIFIER_ID)
        self.helper.add_el(catalog, 'ИдКлассификатора', self.CLASSIFIER_ID)
        self.helper.add_el(catalog, 'Наименование', 'Каталог товаров')
        self.add_owner(catalog)
        products = self.helper.add_el(catalog, 'Товары', 'notext')
        return products

    def add_product(self, name, parent, bar_code, vendor_code, category_id, desc):
        name_with_bar_code = "%s/%s" % (name, vendor_code)
        product_groups = self.product_groups.get(name_with_bar_code, None)
        if product_groups is None:
            product_el = self.helper.add_el(parent, 'Товар', 'notext')
            self.helper.add_el(product_el, 'Ид', self.product_name(self.product_last_pk))
            self.helper.add_el(product_el, 'Штрихкод', bar_code)
            vendor_code = '%s-%s' % ('article-', self.product_last_pk) if vendor_code is None or len(
                vendor_code) < 2 else vendor_code
            self.helper.add_el(product_el, 'Артикул', vendor_code)
            self.helper.add_el(product_el, 'Наименование', name)
            self.add_base_one_price(product_el)
            groups = self.helper.add_el(product_el, 'Группы', 'notext')
            self.helper.add_el(groups, 'Ид', category_id)
            self.helper.add_el(product_el, 'Описание', desc)
            tax_rates = self.helper.add_el(product_el, 'СтавкиНалогов', 'notext')
            tax_rate = self.helper.add_el(tax_rates, 'СтавкаНалога', 'notext')
            self.helper.add_el(tax_rate, 'Наименование', 'НДС')
            self.helper.add_el(tax_rate, 'Ставка', 'Без НДС')

            requisites = self.helper.add_el(product_el, 'ЗначенияРеквизитов', 'notext')
            self.add_requisite(requisites, 'ВидНоменклатуры', 'Товар')
            self.add_requisite(requisites, 'ТипНоменклатуры', 'Товар')
            self.add_requisite(requisites, 'Полное наименование', name)
            self.product_groups[name_with_bar_code] = groups
            self.product_last_pk += 1
            return True
        else:
            self.helper.add_el(self.product_groups[name_with_bar_code], 'Ид', category_id)
            return False

    def get_id_last_product(self):
        return self.product_last_pk-1

    def add_owner(self, parent):
        owner = self.helper.add_el(parent, 'Владелец', 'notext')
        self.helper.add_el(owner, 'Ид', self.SHOP_ID)
        self.helper.add_el(owner, 'Наименование', self.SHOP_NAME)
        self.helper.add_el(owner, 'ОфициальноеНаименование', self.SHOP_NAME)
        self.helper.add_el(owner, 'ОКПО', self.SHOP_OKPO)


class OfferCommerceExport(CommerceExport):
    PACKAGE_ID = 'offer-package-id-1'
    PACKAGE_NAME = 'Пакет предложений'
    PRICE_TYPE_ID = 'price-type-id-1'
    PRICE_TYPE_NAME = 'Сайт'
    CURRENCY = 'RUB'
    TEMPLATE_NAME = FILENAME_OFFERS_TEMPLATE_XML
    STOCK_ID = '123123-sadasda-sdfds-21312-stock-id'

    def add_common(self):
        self.add_package_offers()

    def add_stock(self, parent):
        stock = self.helper.add_el(parent, 'Склад', 'notext')
        self.helper.add_el(stock, 'Ид', self.STOCK_ID)
        self.helper.add_el(stock, 'Наименование', self.SHOP_NAME)

    @staticmethod
    def get_offer_id(pk):
        return 'offer-id-%d' % pk

    def add_package_offers(self):
        package = self.helper.add_el(None, 'ПакетПредложений', 'notext')
        self.helper.add_attr2(package, 'СодержитТолькоИзменения', 'false')
        self.helper.add_el(package, 'Ид', self.PACKAGE_ID)
        self.helper.add_el(package, 'Наименование', self.PACKAGE_NAME)
        self.helper.add_el(package, 'ИдКаталога', self.CATALOG_ID)
        self.helper.add_el(package, 'ИдКлассификатора', self.CLASSIFIER_ID)
        self.add_owner(package)
        price_types = self.helper.add_el(package, 'ТипыЦен', 'notext')
        price_type = self.helper.add_el(price_types, 'ТипЦены', 'notext')
        self.helper.add_el(price_type, 'Ид', self.PRICE_TYPE_ID)
        self.helper.add_el(price_type, 'Наименование', self.PRICE_TYPE_NAME)
        self.helper.add_el(price_type, 'Валюта', self.CURRENCY)
        tax = self.helper.add_el(price_type, 'Налог', 'notext')
        self.helper.add_el(tax, 'Наименование', 'НДС')
        self.helper.add_el(tax, 'УчтеноВСумме', 'true')
        self.helper.add_el(tax, 'Акциз', 'false')
        stocks = self.helper.add_el(package, 'Склады', 'notext')
        self.add_stock(stocks)
        self.add_offers(package)

    def add_offers(self, parent):
        self.groups = self.helper.add_el(parent, 'Предложения', 'notext')

    def add_stock_to_offer(self, offer, offer_db):
        stock = self.helper.add_el(offer, 'Склад', 'notext')
        attr = {'ИдСклада': offer_db.provider.id_1c, 'КоличествоНаСкладе': str(offer_db.count)}
        self.helper.add_attr(stock, attr)

    def add_prices(self, parent, price_val):
        if price_val in (None, ''):
            price_val = 0
        prices = self.helper.add_el(parent, 'Цены', 'notext')
        price = self.helper.add_el(prices, 'Цена', 'notext')
        self.helper.add_el(price, 'Представление', "%s RUB за PCE" % price_val)
        self.helper.add_el(price, 'ИдТипаЦены', self.PRICE_TYPE_ID)
        self.helper.add_el(price, 'ЦенаЗаЕдиницу', price_val)
        self.helper.add_el(price, 'Валюта', self.CURRENCY)
        self.helper.add_el(price, 'Единица', 'PCE')
        self.helper.add_el(price, 'Коэффициент', 1)

    def add_offer(self, id_product, vendor_code, product_name, price, count):
        offer_el = self.helper.add_el(self.groups, 'Предложение', 'notext')
        self.helper.add_el(offer_el, 'Ид', self.product_name(id_product))
        self.helper.add_el(offer_el, 'Артикул', vendor_code)
        self.helper.add_el(offer_el, 'Наименование', product_name)
        self.add_base_one_price(offer_el)
        self.add_prices(offer_el, price)
        self.helper.add_el(offer_el, 'Количество', count)
        stock = self.helper.add_el(offer_el, 'Склад', 'notext')
        attr = {'ИдСклада': self.STOCK_ID, 'КоличествоНаСкладе': str(count)}
        self.helper.add_attr(stock, attr)
        return offer_el