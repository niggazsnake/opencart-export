# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
from lxml import etree


class XMLHelper(object):
    def __init__(self, root_name='xml', namespaces=None, root_attr=None):
        self.root_el = etree.Element(root_name, nsmap=namespaces)
        self.xml = self.root_el
        self.last_add_el = self.root_el
        if root_attr:
            for key, value in root_attr.iteritems():
                self.add_attr2(self.root_el, key, value)

    @staticmethod
    def add_attr(el, attr):
        if attr:
            for key, value in attr.iteritems():
                el.attrib[key] = value

    def add_attr2(self, el, key, value):
        if el is None:
            el = self.root_el
        el.attrib[key] = value

    def add_el(self, parent, name, text):
        if parent is None:
            parent = self.last_add_el
        if text is not None:
            if type(text) not in (unicode, str):
                text = str(text)

            el = etree.SubElement(parent, name)
            if text != "notext":
                el.text = text
            return el
        return None

    def add_dict(self, parent=None, d=None, attr=None):
        if parent is None:
            parent = self.root_el
        if type(d) == dict:
            for key, value in d.items():
                if type(value) == dict:
                    attrs = value.get('attr', None)
                    if attrs:
                        for key_attr, value_attr in attrs.items():
                            for attr in value_attr:
                                node = self.add_el(parent, key, "notext")
                                self.add_attr2(node, key_attr, attr)
                    else:
                        node = self.add_el(parent, key, "notext")
                        self.add_attr(node, attr)
                        self.add_dict(node, value)
                elif type(value) == list:
                    for el in value:
                        if type(el) == dict:
                            node = self.add_el(parent, key, "notext")
                            self.add_dict(node, el)
                        else:
                            self.add_el(parent, key, el)
                else:
                    self.add_el(parent, key, value)

    def add_object(self, obj):
        xml_object = etree.SubElement(self.xml, obj)
        self.add_dict(xml_object, obj)

    def get_xml(self):
        return etree.tostring(self.xml, pretty_print=True, xml_declaration=True, encoding="UTF-8")

    def save_xml(self, path):
        if os.path.exists(path):
            out_file = open(path, 'ab')
            et = etree.ElementTree(self.root_el)
            et.write(out_file, pretty_print=True, xml_declaration=True, encoding="UTF-8")
