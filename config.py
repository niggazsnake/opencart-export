# -*- coding: utf-8 -*-
from __future__ import unicode_literals

EXPORT_SITE_URL = "http://dolmoto.ru/export/exchange1c.php"
EXPORT_SITE_USERNAME = "exchange"
EXPORT_SITE_PASSWORD = "exchange"

# EXPORT_SITE_URL = "http://10.10.11.151/export/exchange1c.php"
# EXPORT_SITE_USERNAME = "exchange"
# EXPORT_SITE_PASSWORD = "exchange"
FILENAME_CATALOG_TEMPLATE_XML = 'import0_2.xml'
FILENAME_OFFERS_TEMPLATE_XML = 'offers0_1.xml'
ROOT_TEMP_FOLDER = 'media'
IMPORT_TEMP_FOLDER = 'import'
EXPORT_TEMP_FOLDER = 'export'
ARCHIVES_TEMP_FOLDER = 'archives'
IMAGES_BASE_FOLDER = 'import_files'
COUNT_PRODUCTS_IN_EXPORT = 10000
DONOR_SITE_URL = 'http://www.rusmechanika.ru'