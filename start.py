# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import csv
import requests
from archivies import Archive
from config import DONOR_SITE_URL
from exports import CommerceExport, OfferCommerceExport
from send import RequestExport


def unicode_csv_reader(utf8_data, dialect=csv.excel, **kwargs):
    csv_reader = csv.reader(utf8_data, dialect=dialect, **kwargs)
    for row in csv_reader:
        yield [unicode(cell, 'utf-8') for cell in row]


def download_images(files):
    for file_image in files:
        url = "%s/assets/shop/_scheme/%s" % (DONOR_SITE_URL, file_image)
        r = requests.get(url, stream=True)
        if r.status_code == 200:
            path = os.path.join('media', 'swf', file_image)
            with open(path, 'wb') as f:
                for chunk in r:
                    f.write(chunk)


def start():
    filename = 'in/shop_package_entry2.csv'
    filename_sql = 'out/update.sql'
    list_files = []
    with open(filename) as csv_file, open(filename_sql, 'w') as file_sql:
        index_arch = 1
        reader = unicode_csv_reader(csv_file)

        catalog = CommerceExport(index_arch)
        offers = OfferCommerceExport(index_arch)
        for pk, category_child, category_parent, bar_code, pk_product, name_product, price, filename, pos, desc in reader:
            count = 1 if price != 'NULL' else 0
            bar_code = bar_code if bar_code != 'NULL' else ''
            price = price if price != 'NULL' else ''
            category_id = catalog.add_category(category_parent, category_child)
            pos = 'Позиция: %s' % pos if pos and pos != "NULL" else ''
            desc = desc if desc and desc != "NULL" else ''
            desc = "%s\n%s" % (pos, desc)
            res = catalog.add_product(name_product, catalog.products, '', bar_code, category_id, desc)
            if res:
                offers.add_offer(catalog.get_id_last_product(), bar_code, name_product, price, count)

            if filename not in list_files:
                list_files.append(filename)
                file_sql.write("UPDATE TABLE SET PIC='%s' WHERE id='%s'\n" % (filename, category_id))

        # download_images(list_files)
        catalog.add_categories_to_xml()
        catalog.save_xml()
        offers.save_xml()
        arch = Archive(catalog.get_save_path())
        arch.create_archive()
        r = RequestExport(arch.path_saved)
        r.send_start_import('offers0_%d.xml' % index_arch)


start()
