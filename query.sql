SELECT distinct shop_module.id AS kd_id, shop_module.name, shop_package.name, shop_part.part_id AS article, shop_part.id AS id, shop_part.name,
(select TRUNCATE(retail,2) from shop_price as sh2 where sh2.part_id=shop_price.part_id ORDER BY valid_from DESC LIMIT 1),
shop_module.pic, shop_module_entry.pos, shop_part.rem
FROM shop_package_entry
LEFT JOIN shop_module ON shop_module.id=shop_package_entry.module_kd_id
LEFT JOIN shop_package ON shop_package.id=shop_package_entry.package_id
LEFT JOIN shop_module_entry ON shop_module_entry.module_id=shop_module.id
LEFT JOIN shop_part ON shop_module_entry.part_id=shop_part.id
LEFT JOIN shop_price ON shop_price.part_id=shop_part.id
WHERE shop_package.status='active' and shop_package.restricted IS NULL