# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import requests
from config import EXPORT_SITE_URL, EXPORT_SITE_PASSWORD, EXPORT_SITE_USERNAME


class RequestExport(object):
    HEADERS = {'content-type': 'application/x-www-form-urlencoded'}

    def __init__(self, name_archive, log=None):
        self.log = log
        self.url = EXPORT_SITE_URL
        self.username = EXPORT_SITE_USERNAME
        self.password = EXPORT_SITE_PASSWORD
        self.name_archive = name_archive
        self.name_xml = self.get_name_xml_from_arch()
        self.log_message = []
        self.cookies = {}
        self.check_auth()
        self.send_init()
        self.send_file()
        self.send_start_import()
        self.save_log()

    def get_name_xml_from_arch(self):
        if self.name_archive:
            name, ext = os.path.splitext(os.path.basename(self.name_archive))
            return "%s.xml" % name
        return None

    def save_log(self):
        message = '\n'.join(self.log_message)
        if self.log:
            self.log.add_message('отправка архива на загрузку', message)
        print(message)

    def make_url(self, params):
        url_params = None
        for key, value in params.iteritems():
            url_params = "%s=%s" % (key, value) if url_params is None else "%s&%s=%s" % (url_params, key, value)
        if url_params:
            return "%s?%s" % (self.url, url_params)
        return self.url

    def send_request(self, url, _file=None, auth=None):
        params = {'url': url, 'data': _file, 'cookies': self.cookies, 'headers': self.HEADERS, 'verify': False}
        if auth:
            params.update({'auth': (self.username, self.password)})
        r = requests.post(**params)

        self.log_message.append("урл-%s, code-%s, text-%s" % (r.text, r.status_code, r.text))
        return r.text

    def parse_answer_auth(self, answ):
        if answ:
            answ = answ.split('\n')
            if answ and len(answ) > 2:
                self.cookies = {'key': answ[2]}
                self.log_message.append('установлены куки-%s' % answ[2])
            else:
                self.log_message.append('слишком короткий ответ')
        else:
            self.log_message.append('нет ответа')

    def check_auth(self):
        url = self.make_url({'mode': 'checkauth', 'type': 'catalog'})
        r = self.send_request(url, auth=True)
        self.log_message.append(r)
        self.parse_answer_auth(r)

    def send_init(self):
        url = self.make_url({'mode': 'init', 'type': 'catalog'})
        r = self.send_request(url)

    def send_file(self):
        if os.path.exists(self.name_archive):
            self.log_message.append('прикрепляю архив -%s' % self.name_archive)
            filename = os.path.basename(self.name_archive)
            fl = open(self.name_archive, 'rb')
            url = self.make_url({'mode': 'file', 'type': 'catalog', 'filename': filename})
            r = self.send_request(url, fl)
        else:
            self.log_message.append('архив -%s не найден' % self.name_archive)

    def send_start_import(self, name_xml=None):
        name_xml = name_xml if name_xml else self.name_xml
        url = self.make_url({'mode': 'import', 'type': 'catalog', 'filename': name_xml})
        r = self.send_request(url)
        print r

